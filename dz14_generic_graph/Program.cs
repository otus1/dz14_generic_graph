﻿using dz14_generic_graph.Model;
using System;

namespace dz14_generic_graph
{
    class Program
    {
        static void Main(string[] args)
        {
            bool res;
            var graph = new Graph<string>();
            graph.Add(new Node<string>(1, "A"));
            graph.Add(new Node<string>(2, "B"));
            graph.Add(new Node<string>(3, "C"));
            graph.Add(new Node<string>(4, "D"));
            graph.Add(new Node<string>(5, "E"));
            graph.Add(new Node<string>(6, "F"));
            graph.Add(new Node<string>(7, "G"));

            Console.WriteLine("=== List ===");
            foreach (var el in graph.GetList())
            {
                Console.WriteLine(el);
            }
            Console.WriteLine("============");

            var nodeGet = graph.GetById(2);
            Console.WriteLine("\nGet (id = 2): " + nodeGet);

            var nodeContain = graph.GetById(1);
            res = graph.Contains(nodeContain);                
            Console.WriteLine($"Contain S({nodeContain}) result = {res}");

            var nodeRemove = graph.GetById(2);
            res = graph.Remove(nodeRemove);
            Console.WriteLine($"remove ({nodeRemove}) result = {res}");

            
            Console.WriteLine("\n=== List ===");
            foreach (var el in graph.GetList())
            {
                Console.WriteLine(el);
            }
            Console.WriteLine("============");

            Console.WriteLine("\n=== List(5) ===");
            foreach (var el in graph.GetList5())
            {
                Console.WriteLine(el);
            }
            Console.WriteLine("============");

            Console.ReadKey();
        }
    }
}

