﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dz14_generic_graph.Model
{
    public interface INode<T>
    {   
        public int Id { get; set; }
        public T Data { get; set; }
    }
}
