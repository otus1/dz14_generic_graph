﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dz14_generic_graph.Model
{
    public class Node<T> : INode<T>
    {
        public int Id { get; set; }
        public T Data { get; set; }

        public Node(int id, T data)
        {
            Id = id;
            Data = data;
        }

        public override string ToString()
        {
            return $"Id={Id}; {Data}";
        }
    }
}
