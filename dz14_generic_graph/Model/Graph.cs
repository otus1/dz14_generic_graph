﻿using System;
using System.Collections.Generic;
using System.Text;
using dz14_generic_graph.Model;
using System.Linq;

namespace dz14_generic_graph.Model
{
    public class Graph<T>
    {
        public List<Node<T>> Nodes { get; set; }

        public Graph()
        {
            Nodes = new List<Node<T>>();
        }
        public bool Add(Node<T> node)
        {
            var result = false;
            if (!Contains(node))
            {
                Nodes.Add(node);
                result = true;
            }
            return result;
        }

        public bool Contains(Node<T> node)
        {
            var x = Nodes.Contains(node);
            return x;
        }

        public bool Remove(Node<T> node)
        {
            var result = false;
            if (Contains(node))
            {
                Nodes.Remove(node);
                result = true;
            }
            return result;
        }

        public Node<T> GetById(int id)
        {
            return Nodes.FirstOrDefault(x => x.Id == id);
        }
        public List<Node<T>> GetList()
        {
            return Nodes;
        }
        public List<Node<T>> GetList5()
        {
            return Nodes.Take(5).ToList();
        }

    }
}
